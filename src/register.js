import {HttpClient} from 'aurelia-http-client';
import {Router} from 'aurelia-router';

export class Register {
	static inject() { return [HttpClient, Router]; }
	constructor(http, router){
		this.http = http;
		this.router=router;
		this.errors = {};
	}

	register(){
		
		if(!this.validateRegisterForm())
			return;
		this.registerbackendCall(this.register.username, this.register.password);
		return;
	}
	registerbackendCall(username, password){
		this.http
			.createRequest(`${config.APIhost}/register`)
			.asPost()
			.withContent({
				'username':username,
				'password':password
			})
			.withHeader('Content-Type', 'application/json')
			.send()
			.then( (d) => {
				this.errors.register = null;
				console.log(d);
				// EXECUTE LOGIN
				this.loginBackendCall(username, password);
			})
			.catch( (e) => {
				console.log(e);
				switch(e.response){
					case 'invalid.password' : 
						this.errors.register = {
							type : 'warning',
							message: 'Le formulaire est mal rempli.',
							details:'Votre mot de passe n\'est pas valide'
						};
						break;
					default : 
						this.errors.register = {
							type : 'danger',
							message: 'Erreur.',
							details:'Veuillez réessayer plus tard.'
						};
				}
			});
	}
	validateRegisterForm(){

		if(stringNullOrEmpty(this.register.username)||stringNullOrEmpty(this.register.password)){
			this.errors.register = {
				type : 'warning',
				message: 'Le formulaire est mal rempli.',
				details:'Tous les champs sont obligatoires.'
			};
			return false;
		}
		if(this.register.password!=this.register.password2){
			this.errors.register = {
				type : 'warning',
				message: 'Le formulaire est mal rempli.',
				details:'Les deux mots de passe ne correspondent pas.'
			};
			return false;
		}

		return true;
	}

	login(){
		if(!this.validateLoginForm())
			return;
		this.loginBackendCall(this.login.username, this.login.password);
		return;
	}
	loginBackendCall(username, password){
		this.http
			.createRequest(`${config.APIhost}/login`)
			.asPost()
			.withContent({
				'username':username,
				'password':password
			})
			.withHeader('Content-Type', 'application/json')
			.send()
			.then( (d) => {
				console.log(d);
				this.errors.login = null;
				// TODO - Est-ce qu'on peut faire mieux?
				session = {
					token : d.content.token,
					name : username,
					type : 'member'
				}
				saveSession();
				//window.sessionStorage.setItem('token',d.content.token);
				//console.log(window.sessionStorage.getItem('token'))
				console.log(session.token);
				this.router.navigate('game');
			})
			.catch( (e) => {
				console.log(e);
				// TODO - Changer les erreurs
				switch(e.response){
					case 'invalid.password' : 
						this.errors.login = {
							type : 'warning',
							message: 'Le formulaire est mal rempli.',
							details:'Votre mot de passe n\'est pas valide'
						};
						break;
					case 'invalid.username' : 
						this.errors.login = {
							type : 'warning',
							message: 'Le formulaire est mal rempli.',
							details:'Votre nom d\'utilisateur n\'est pas valide'
						};
						break;
					default : 
						this.errors.login = {
							type : 'danger',
							message: 'Erreur.',
							details:'Veuillez réessayer plus tard.'
						};
				}
			});
	}
	validateLoginForm(){

		if(stringNullOrEmpty(this.login.username)||stringNullOrEmpty(this.login.password)){
			this.errors.login = {
				type : 'warning',
				message: 'Le formulaire est mal rempli.',
				details:'Tous les champs sont obligatoires.'
			};
			return false;
		}

		return true;
	}

	canActivate(){
		return (stringNullOrEmpty(session.type) || session.type=="guest");
	}
	activate(params, querystring, routeconfig){
		if(!stringNullOrEmpty(params.name))
			this.register.username=params.name;
	}
}