import {Router} from 'aurelia-router';
import {HttpClient} from 'aurelia-http-client';

export class App {
	static inject() { return [Router, HttpClient]; }
	constructor(router,http){
		this.router = router;
		this.http=http;
		
	    this.router.configure(config => {
			config.title = 'Galactronix';
			config.addPipelineStep('authorize', AuthorizeStep);
			config.map([
				{ route: ['','home'], 					moduleId: 'home'},
				{ route: 'game', 						moduleId: 'game',	 auth:true},
				{ route: ['register','register/:name'], moduleId: 'register'}
			]);
	    }); 

	}

	authenticate(){
		this.http
			.createRequest(`${config.APIhost}/login`)
			.asPost()
			.withContent({
				'username':this.username,
				'password':this.password
			})
			.withHeader('Content-Type', 'application/json')
			.send()
			.then( (d) => {
				console.log(d);
				this.loginerror = null;
				
				session = {
					token : d.content.token,
					name : this.username,
					type : 'member'
				}
				saveSession();
				this.router.navigate('game');
			})
			.catch( (e) => {
				console.log(e);
				
				switch(e.response){
					case 'invalid.password' : 
						this.loginerror = {
							type : 'warning',
							message: 'Le formulaire est mal rempli.',
							details:'Votre mot de passe n\'est pas valide'
						};
						break;
					case 'invalid.username' : 
						this.loginerror = {
							type : 'warning',
							message: 'Le formulaire est mal rempli.',
							details:'Votre nom d\'utilisateur n\'est pas valide'
						};
						break;
					default : 
						this.loginerror = {
							type : 'danger',
							message: 'Erreur.',
							details:'Veuillez réessayer plus tard.'
						};
				}
			});
	}

	navigateAwayFromRegisterButton(){
		var tempUserName = this.username;
		this.username="";
		this.password="";
		if(!stringNullOrEmpty(tempUserName)){
			this.router.navigate('register/'+tempUserName);
		}else{
			this.router.navigate('register');
		}
		
	}
}

class AuthorizeStep {
  run(routingContext, next) {
    // Check if the route has an "auth" key
    // The reason for using `nextInstructions` is because
    // this includes child routes.
    if (routingContext.nextInstructions.some(i => i.config.auth)) {
      var isLoggedIn = !stringNullOrEmpty(session.token);
      if (!isLoggedIn) {
        return next.cancel(new Redirect('register'));
      }
    }

    return next();
  }
}