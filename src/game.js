import {computedFrom} from 'aurelia-framework';

export class Game {
	constructor(){
        this.quitMsg = 'Voulez-vous vraiment quitter la partie?';
        this.player = {
            hand:[]
        };
        this.round = {
            question:{
                a_count:3
            },
            answers:[],
            czarId:null
        };
        this.lobby = {
            users : []
        };
        this.gameError = null; 
	}
 
    //@computedFrom('player', 'round')
    get currentlyCzar(){
        return (this.player.id == this.round.czarId);
    }

    //@computedFrom('lobby')
    get canStart(){
        let users = this.lobby.users;
        return (users != undefined && users.length >= this.lobby.gameOption.player_min);
    }

    
    //@computedFrom('selectedCards', 'round')
    get canPlay(){
        return (this.selectedCards.length == this.round.question.a_count );
    }

    //@computedFrom('player')
    get selectedCards(){
        let selectedCards = [];
        let hand = this.player.hand;
        for(let i = 0; i<hand.length; i++){
            if(hand[i].selected){
                selectedCards.push(hand[i].id);
            }
        }
        return selectedCards;
    }

    //@computedFrom('lobby') 
    get played(){ 
        let users = this.lobby.users;
        for (let i = 0; i<users.length; i++){
            if(users[i].playerId == this.player.id && users[i].played){
                return true;
            }
        }
        return false;
    }

    //@computedFrom('lobby.answers') 
    get hasAnswer(){
        let answers = this.round.answers;
        return answers != undefined && answers.length > 0;
    }

    /*
        Émet un évenment pour lancer la partie.
    */
    start(){
        socket.emit('start', this.lobby.id);
    }

    /*
        Sélectionne l'identifiant d'une carte.
    */
    select(id){
        // Si le joueur n'est pas le Tsar
        if(!this.currentlyCzar){
            // TODO - Faire mieux?
            // Trouver si l'index est existant dans selectedCards
            let index = this.selectedCards.indexOf(id);
            // Si l'identifiant de la carte ne se trouve pas dans this.selectedCards
            if(index == -1){
                // Si la taille du tableau n'excède pas le nombre de réponses demandées.
                if(this.selectedCards.length < this.round.question.a_count){
                    // Ajouter à this.selectedCards
                    for(let i = 0; i<this.player.hand.length; i++){
                        if(this.player.hand[i].id==id){
                            this.player.hand[i].selected = true;
                            //this.selectedCards.push(id);
                        }
                    }
                }else{
                    //Error
                } 
            }else{ 
                // Enlever la carte de selectedCards
                //this.selectedCards.splice(index, 1);
                for (let i = 0; i < this.player.hand.length; i++) {
                    if(this.player.hand[i].id==id){
                        this.player.hand[i].selected = false;
                    }
                };
            }
        }else{
            //Error
        }
    }

    play(id){
        // Si le joueur est le Csar
        if(!this.currentlyCzar){
            // Si le nombre de carte selectionner et équivalent au nombre de réponse pour la question.
            if(this.canPlay){
                let data = {
                    roundId:this.round.id,
                    playerId:this.player.id,
                    cardIds:this.selectedCards
                };
                console.log(data);
                // Émettre l'événement play
                socket.emit('answer', data);
            }
        }
    }

    vote(id){
        // Si le joueur est le Csar
        if(this.currentlyCzar){
            console.log(id);
            // Émettre l'événement vote
            socket.emit('vote', {
                answerId:id
            });
        }else{
            // Error
        }
    }

    activate(){
        // TEMPORAIRE {
        // TODO - Si le bogue est corrigé, utiliser le package loader de ES6.
        socket = 
        io
        .connect(config.APIhost + "/", {
            'forceNew':true,
            'query':'token=' + session.token
        })
        .on('connect', (d) => {
            console.log('WebSocket : ON connect => Data :')
            console.log(d);
        }) 
        /*
            Est émis lorsque l'utilisateur courrant se connecte.
            Envoie l'état du lobby.
        */
        .on('join', (d) => {
            this.lobby = d;
            console.log('WebSocket : ON join => Lobby :');
            console.log(this.lobby);
        })
        /*
            Est émis lorsqu'un joueur se connecte au lobby.
            Envoi l'état du lobby.
        */
        .on('joined', (d) => { 
            this.lobby = d;
            console.log('WebSocket : ON joined => Lobby :');
            console.log(this.lobby);
        })
        /*
            Est émis lorsqu'un nouveau round commence.
            Envoi l'ensemble des rounds joués.
        */
        .on('started', (d) => {
            let users = this.lobby.users;
            if(d.length == 1){
                for(let i = 0; i<users.length; i++){
                    users[i].points = 0;
                }
            }
            this.round = d; 
            console.log('WebSocket : ON started => Round array :'); 
            console.log(d);
            // Initialiser les points des joueur
            // Initilaise la varible played a false.
            for(let i  = 0; i<users.length; i++){
                users[i].played = false;
            }
        })
        /*
            Est émis lorsqu'une partie est commencée.
            Envoie la main du joueur.
            Suggestion : Handed et Mapped pourraient être émit en même temps.
        */
        .on('handed', (d) => {
            /*
                id
                userId
                hand
            */
            this.player.id = d.id;
            this.player.userId = d.userId
            if(this.player.hand.length == 0){
                this.player.hand = d.hand;
            }else{
                for(let i = 0; i<d.hand.length; i++){ 
                    this.player.hand[i] = d.hand[i];
                }
            }
            //this.player = d; 
            console.log('WebSocket : ON handed => Player :');
            console.log(this.player);
        })
        /*
            Est émis lorsqu'une partie est commencée.
            Permet de mapper les utilisateurs avec leur identifiant de joueur.
            Suggestion : Handed et Mapped pourraient être émit en même temps.
        */
        .on('mapped', (d) => {
            console.log('WebSocket : ON mapped => User mapping : ');
            console.log(d);
            for(let i  = 0; i<this.lobby.users.length; i++){
                let userId = this.lobby.users[i].id;
                if(d[userId] != null){
                    this.lobby.users[i].playerId = d[userId].playerId;
                    this.lobby.users[i].points = d[userId].score;
                }else{
                    console.log('Erreur => Identifiant inexistant dans le mapping.');
                }
            } 
        })
        /*
            Est émis lorsqu'un joueur joue une carte.
            Envoie l'identifiant de ce joueur.
        */
        .on('answered', (d) => {
            console.log('WebSocket : ON answered => Player id : ');
            console.log(d);
            for(let i = 0; i<this.lobby.users.length; i++){
                if(this.lobby.users[i].playerId == d){
                    this.lobby.users[i].played = true;
                }
            } 
        })
        /*
            Est émis lorsque tout les joueurs ont joué.
            Envoie l'ensembles des réponses des joueurs.
        */
        .on('answers', (d) => {
            console.log('WebSocket : ON answers => Answers : ');
            console.log(d);
            this.round.answers = d; 
        })
        /*
            Est émis lorsque le Tsar a voté.
            Envoi le id du joueur qui a eu le point.
        */
        .on('voted', (d) => {
            console.log('WebSocket : ON voted => Object : ');
            console.log(d);
            for(let i = 0; i<this.round.answers.length; i++){
                let answer = this.round.answers[i];
                if(answer.id == d.id){
                    let response = '';
                    for(let j = 0; j<answer.contents.length; j++){
                        if(j>0){
                            response += ', ';
                        }
                        response += answer.contents[j];
                    }
                    this.gameError = {
                        type:'success',
                        message:'Réponse(s) retenue(s):',
                        details:response
                    }
                }
            }
        })
        /*
            Est émis losrque la partie est terminé.
            Renvoie le id de la personne qui a gagnée.
        */
        .on('over', (d) => { 
            console.log('WebSocket : ON over => Data : ');
            console.log(d);
            this.gameError = {
                        type:'success',
                        message:'Partie terminée!',
                        details:'Appuyer sur commencer une partie pour jouer de nouveau.'
                    }
            this.player = {
                hand:[]
            };
            this.round = {
                question:{
                    a_count:3
                },
                answers:[],
                czarId:null
            };
        })
        /*
            Est émis lorsqu'un joueur quitte une partie.
        */
        .on('left', (d) => {
            console.log('WebSocket : ON left => userId : ');
            for(let i = 0; i<this.lobby.users.length; i++){
                if(this.lobby.users[i].playerId = d){
                    this.lobby.users.splice(i, 1);
                }
            } 
        })
        /*
            Est émis lorsqu'une partie est intérompue.
        */
        .on('killed', () => {
            console.log('WebSocket : ON killed');
            this.player = {
                hand:{}
            };
            this.round = {
                question:{
                    a_count:3
                },
                answers:[],
                czarId:null
            };
        })
        /*
            Est émis lorsqu'il y a une erreur.
        */
        .on('error', (d) => {
            console.log('ERROR');
            console.log(d);
            switch(d){
                case 'missing.user' : 
                    this.gameError = {
                        type : 'warning',
                        message: 'Impossible de commencer la partie.',
                        details: `Il n'y a pas assez de peronne dans le lobby. Le minimum permis est de ${this.lobby.gameOption.player_min}.`
                    }
                    break;
                case 'missing.card' :
                    this.gameError = {
                        type : 'warning',
                        message: 'Impossible d\'effectuer cette action.',
                        details: 'La carte est inexistante ou a déjà été jouée.'
                    }
                    break;
                case 'missing.users' :
                    this.gameError = {
                        type : 'warning',
                        message: 'Attention!',
                        details: 'Il manque des joueurs pour débuter la partie.'
                    }
                    break;
            }
        });
        console.log(socket);
        //}TEMPORAIRE
        /*
        window.onbeforeunload = (e) => {
            return this.quitMsg;
        }
        */
    }

    deactivate(){
        socket.disconnect();
        clearSession();
        /*
        if(window.confirm(this.quitMsg)){
            socket.disconnect();
            window.onbeforeunload = null;
        }
        */
    }
}