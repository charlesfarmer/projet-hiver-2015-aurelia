import {customElement} from 'aurelia-framework';

@customElement('esp-chat')
export class EspChat {
	constructor(){
        this.chat = [];
        this.msg = '';

        // TEMPORAIRE
        socket
        .on('chat message', msg => {
            console.debug(msg);
            this.chat.push(msg);
        })
        .on('user connected', msg => {
          console.debug(msg);
          console.log(this);
          this.chat.push(msg);
        })
        .on('user disconnected', msg => {
          console.debug(msg);
          this.chat.push(msg);
        })
        .on('greet', msg => {
          console.debug(msg);
          alert('Welcome ' + msg);
        })
        .on('error', msg => {
          if(msg.usernameInUse){
              console.debug('Username ' + msg.username + ' already in use');
              alert('Username ' + msg.username + ' already in use');
          }
        });
	}

	sendMsg(){
        if(this.msg != ''){
            socket.emit('chat message', this.msg);
            this.msg = '';
        }
	}
}