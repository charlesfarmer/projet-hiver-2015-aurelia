import {computedFrom} from 'aurelia-framework';
import {HttpClient} from 'aurelia-http-client';
import {Router} from 'aurelia-router';

export class Home {
	static inject() { return [HttpClient, Router]; }
	constructor(http, router){
		this.http = http;
		this.router=router;
	}

	@computedFrom('guestUsername')
	get playButtonDisable(){
		return stringNullOrEmpty(this.guestUsername);
	}
	playButtonGuestClicked(){
		console.log("bdjksf;ql");
		this.http
			.createRequest(`${config.APIhost}/guest`)
			.asPost()
			.withContent({
				'username':this.guestUsername
			})
			.withHeader('Content-Type', 'application/json')
			.send()
			.then( (d) => {
				console.log(d);
				session = {
					token : d.content.token,
					name : this.guestUsername,
					type : 'guest'
				}
				saveSession();
				this.router.navigate('game');
			})
			.catch( (e) => {
				console.log(e);
				// TODO - Changer les erreurs
				alert("Erreur lors de la connexion en tant qu'invité")
			});
	}
}