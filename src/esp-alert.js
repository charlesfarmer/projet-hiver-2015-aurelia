import {customElement, bindable} from 'aurelia-framework';
   
@customElement('esp-alert')
export class EspAlert {
	@bindable model;

	dismiss(){
		this.model = null;
	}
}